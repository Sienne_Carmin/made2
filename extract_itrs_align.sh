#!/bin/bash
##### Script written by Sarah Guinchard, august 2021
##### Exctrats MADE2 ITRs and aligns them with clustalo

##### VARIABLES #####
DATA_PATH=/path/to/data/directory
NAME=data_name


##### SCRIPT #####
# Get bed files for ITR extraction
echo "Extracting fasta headers..."
sed -e 's/:/-/g' -i $DATA_PATH/$NAME\.fa
sed -e 's/-//g' -i $DATA_PATH/$NAME\.fa
echo "Creating bed file..."
awk 'sub(/^>/, "")' $DATA_PATH/$NAME\.fa | sed -e 's/$/\t10\t37/' > $DATA_PATH/$NAME\_itr.bed


# Extract sequences and reverse 3' ITRs
echo "Extracting left ITR..."
bedtools getfasta -fi $DATA_PATH/$NAME\.fa -bed $DATA_PATH/$NAME\_itr.bed -fo $DATA_PATH/$NAME\_5itr.fa
echo "Reversing the sequences..."
revseq -sequence $DATA_PATH/$NAME\.fa -outseq $DATA_PATH/$NAME\_revcomp.fa
sed -e 's/_/\//' -i $DATA_PATH/$NAME\_revcomp.fa
sed -e 's/[ :]//g' -i $DATA_PATH/$NAME\_revcomp.fa
awk 'sub(/^>/, "")' $DATA_PATH/$NAME\_revcomp.fa | sed -e 's/$/\t10\t37/' > $DATA_PATH/$NAME\_itr.bed

echo "Extracting right ITR in revcomp..."
bedtools getfasta -fi $DATA_PATH/$NAME\_revcomp.fa -bed $DATA_PATH/$NAME\_itr.bed -fo $DATA_PATH/$NAME\_3itr.fa

echo "Merging the files..."
cat $DATA_PATH/$NAME\_5itr.fa $DATA_PATH/$NAME\_3itr.fa > $DATA_PATH/$NAME\_all_itrs.fa

clustalo -i $DATA_PATH/$NAME\_all_itrs.fa -o $DATA_PATH/$NAME\_all_itrs_aligned.fa
echo "If necessary, please cut the alignment to ITR size by running the following commands after replacing START and END with the cutting points:
    - awk 'sub(/^>/, "")' $DATA_PATH/$NAME\.fa | sed -e 's/$/\tSTART\tEND/' > $DATA_PATH/cut_$NAME\_ITR_alignment.bed
    - bedtools getfasta -fi $DATA_PATH/$NAME\_all_itrs_aligned.fa -bed $DATA_PATH/cut_$NAME\_ITR_alignment.bed -fo $DATA_PATH/$NAME\_all_itrs_aligned_cut.fa"

# Cleanup directory from temp files
rm $DATA_PATH/$NAME\_revcomp.fa.fai
rm $DATA_PATH/$NAME\.fa.fai
rm $DATA_PATH/$NAME\_5itr.fa
rm $DATA_PATH/$NAME\_3itr.fa
rm $DATA_PATH/$NAME\_revcomp.fa
rm $DATA_PATH/$NAME\_itr.bed