#!/bin/bash

### Source and activate your conda environment here if necessary:
# . /local/env/envconda.sh
# conda activate rm_shuffle

##### VARIABLES #####
### Fill in as necessary
PATHTOSHUFFLE=/path/to/fasta-shuffle-letters
SEQUENCE=/path/to/consensus/fasta/file
PATHTORM=RepeatMasker
CHROMREP=/path/to/fasta/chromosomes/directory
OUTREP=/path/to/output/root/directory
OUTPUTDIR=RM_results

##### SCRIPT #####
for k in {3..7..2}
do
        echo "k = $k"
        mkdir $OUTREP/shuffled_hg38_k$k
        for FASTA in {1..22} X Y
        do
                echo "chr = $FASTA "
                $PATHTOSHUFFLE -kmer $k $CHROMREP/chr$FASTA\.fa $OUTREP/shuffled_hg38_k$k/shuffled_chr$FASTA\_k$k.fa
        done
        echo 'Chromosomes concatenation after shuffling...'
        cat $OUTREP/shuffled_k$k/* > $OUTREP/shuffled_hg38_k$k.fa
        wc -l $OUTREP/shuffled_hg38_k$k.fa
        echo 'Running RepeatMasker.'
        time $PATHTORM $OUTREP/shuffled_hg38_k$k.fa -lib $SEQUENCE -nolow -no_is -no_id -a -dir $OUTPUTDIR/RM_shuffled_hg38_k$k
done

#Script by Lucie Baget (feb 2021) edited by Sarah Guinchard (apr 2021)
