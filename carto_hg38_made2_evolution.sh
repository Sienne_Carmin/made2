#!/bin/bash

### Fill as neccessary:
ANNOTATION_EVOL=path/to/divergence/annotation.bed
SEQUENCE_PATH=path/to/sequence/data/repo
OUTPUT_PATH=path/to/output/dir
ANNO_NAME=""
GROUP_NAME=""

# Extract annotation name for the outpout file:
ANNO_NAME=$(basename $ANNOTATION_EVOL)
ANNO_NAME=${ANNO_NAME:0:((${#ANNO_NAME}-4))}
for SEQGROUP in $SEQUENCE_PATH/hg38*.bed
do
  # Extract squences group name for the outpout file:
  GROUP_NAME=$(basename $SEQGROUP)
  GROUP_NAME=${GROUP_NAME:0:((${#GROUP_NAME}-4))}
  # Check wether the sequences matches an intergenic region:
  intersectBed -a $SEQGROUP -b $ANNOTATION_EVOL -wb | awk '{OFS="\t"; print $4,"'$GROUP_NAME'", $10}' >> $OUTPUT_PATH/$ANNO_NAME\_evo_zone.csv
  awk '{OFS="\t"; print $2 $3}' $OUTPUT_PATH/$ANNO_NAME.csv | sort | uniq -c
done
