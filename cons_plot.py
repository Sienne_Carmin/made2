#! /usr/bin/env python3

####################################################################
# Script for alignement base consistency analysis and illustration #
#    Usage : ./cons_plot.py /path/to/alignment.fa Alignment_name   #
#               Written by Sarah Guinchard, may 2021               #
####################################################################

import sys
import pandas as pd
import matplotlib.pyplot as pl * RepeatMasker search in genomet

def fasta_parser(fasta_file):
    '''Read fasta file line by line and yield fasta header and sequence every time a header
    is encountered.'''
    with open(fasta_file, 'r') as sequences:
        name = ''
        sequence = ''
        for line in sequences.readlines():
            if line.startswith('>'):
                if len(name)>0:
                    yield name,sequence.upper()
                name = line.strip('\n')
                sequence = ''
            else:
                sequence = sequence + line.strip('\n')
        yield name, sequence.upper()


def get_consistency(fasta_file):
    '''Count base occurrence for each position in the alignment and returns the frequency
       of the consensus base.'''
    seq_store = {name : sequence     for name,sequence in fasta_parser(fasta_file)}
    cons_df = pd.DataFrame({'Consensus':[], 'Frequency':[], 'Occupancy':[]})
    for i, base in enumerate(list(seq_store.values())[0]):
        counts = {'A':0, 'C':0, 'G':0, 'T':0}
        for current_seq in seq_store.values():
            counts['A'] += current_seq[i].count('A')
            counts['C'] += current_seq[i].count('C')
            counts['G'] += current_seq[i].count('G')
            counts['T'] += current_seq[i].count('T')
        consensus = max(counts, key=counts.get)
        occupancy = sum(counts.values())/len(list(seq_store.values()))
        cons_df = cons_df.append(dict(zip(cons_df.columns, [consensus, counts[consensus]/len(list(seq_store.values())), occupancy])), ignore_index = True)
    return cons_df

def draw_consistency_plot(cons_df, align_name):
    '''Draws a consistency plot from the data frame computed with get_consistency().
       When a nucleotid is present in more than half of the sequences on a given position,
       it is drawn at the bottom of the plot as well.'''
    fig, axes = plt.subplots(figsize = (18,12))
    plt.rc('xtick', labelsize = 18)
    plt.rc('ytick', labelsize = 18)
    plt.xlabel('Position in the alignment', fontsize = 18)
    plt.ylabel('Frequency of the most present nucleotide', fontsize = 18)
    plt.title(f'{align_name} alignment consistency', fontsize = 24)
    plt.plot(cons_df.index, cons_df['Frequency'], color = 'dimgrey')
    ### Uncomment for consensus sequence printing at the bottom of the plot
    # for i in cons_df.index:
    #     if cons_df['Frequency'][i] > 0.5:
    #         if cons_df['Consensus'][i] == 'A':
    #             plt.text(i, 0.1, cons_df['Consensus'][i], fontsize = 8, ha='center',
    #                      weight = 'semibold', color = 'red')
    #         if cons_df['Consensus'][i] == 'C':
    #             plt.text(i, 0.1, cons_df['Consensus'][i], fontsize = 8, ha='center',
    #                      weight = 'semibold', color = 'darkgreen')
    #         if cons_df['Consensus'][i] == 'G':
    #             plt.text(i, 0.1, cons_df['Consensus'][i], fontsize = 8, ha='center',
    #                      weight = 'semibold', color = 'saddlebrown')
    #         if cons_df['Consensus'][i] == 'T':
    #             plt.text(i, 0.1, cons_df['Consensus'][i], fontsize = 8, ha='center',
    #                      weight = 'semibold', color = 'blue')
    plt.savefig(f'{align_name}_consistency_plot.png', bbox_inches = 'tight')


def get_consensus(cons_df, align_name):
    '''Outputs the consensus for the alignment as a fasta sequence.
    The consensus contains nucleotides with a frequency over 0.50 and N for sites with
    no clear consensus but and occupancy over 50%.'''
    fasta_cons = f'>{align_name}_consensus\n'
    for i in cons_df.index:
        if cons_df['Frequency'][i] > 0.5:
            fasta_cons = fasta_cons + cons_df['Consensus'][i]
        elif cons_df['Occupancy'][i] > 0.5:
            fasta_cons = fasta_cons + 'N'
    return fasta_cons


def main(fasta_file, align_name):
    cons_df = get_consistency(fasta_file)
    with open(f'{align_name}_consensus.fa', 'w') as cons_file:
        cons_file.write(get_consensus(cons_df, align_name))
    draw_consistency_plot(cons_df, align_name)

if __name__=='__main__':
    fasta_file = sys.argv[1]
    align_name = sys.argv[2]
    main(fasta_file, align_name)

