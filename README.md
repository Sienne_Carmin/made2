# Scripts written for Sarah Guinchard's internship at Inria from april to september 2021

## Scripts to be added:
 * Logol models
    * list them here
 * (Venn plots ?)

## Data to be added?

## Scripts added:
* Human genome shuffling (randomize_wg_hg38.sh)
* RepeatMasker search in genome (run_rm.sh)
* Logol parser (logol_parser.py)
* Consplot (cons_plot.py)
* RepeatMasker output to BED (rm_output_to_bed.sh)
* ITR extraction and alignment for logo (extract_itrs_align.sh)
* Carto intra/inter with IntersectBed (carto_genic_hg38_made2.sh)
* Carto genetic divergence with IntersectBed (carto_hg38_made2_evolution.sh)
* Genome cartography: R script (carto_plots.R)