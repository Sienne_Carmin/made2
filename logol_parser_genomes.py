#########################################################################
#        Script to parse a directory containing the Logol output        #
# and save matches to a fasta file as well as a csv table with figures. #
#            Usage : ./logol_parser_genomes.py /path/to/data/directory            #
#                  Written by Sarah Guinchard, may 2021                 #
#########################################################################

import xml.etree.ElementTree as et
import sys


def get_root(data_path):
    '''Opens xml file and reads it to a root object.'''
    print('Hello!\nLets grab that xml file...')
    return et.parse(data_path).getroot()

def is_match(root):
    '''Checks if the xml file contains at least one match.'''
    if root.findall('match'):
        return True
    else:
        return False

def get_fasta_header(root):
    '''Returns the fasta header of the matched sequence.'''
    return root.find('fastaHeader').text

def get_bed(root):
    '''Creates a BED entry for each match in the xml and saves them to a BED file.'''
    bed_content = ""
    chrom = get_fasta_header(root)
    for match in root.findall('match'):
        start = match.find('begin').text
        end = match.find('end').text
        bed_content = bed_content + f'{chrom}\t{start}\t{end}\n'
    with open(f'{chrom}_logol_hits.bed', 'w') as bed_file:
        bed_file.write(bed_content)
        print('Saving BED file...')
    
def get_fasta(root):
    '''Gets the fasta sequence of hits from the xml and savec them to a fasta file.'''
    fasta_content = ""
    chrom = get_fasta_header(root)
    for match in root.findall('match'):
        start = match.find('begin').text
        end = match.find('end').text
        seq = ""
        for variable in match.findall('variable'):
            seq = seq + variable.find('content').text
        fasta_content = fasta_content + f'>{chrom}:{start}-{end}\n{seq}\n'
    with open(f'{chrom}_logol_hits.fa', 'w') as fasta_file:
        fasta_file.write(fasta_content)
        print('Saving Fasta file...')

def main(data_path):
    root = get_root(data_path)
    if is_match(root):
        get_bed(root)
        get_fasta(root)
        print('Have a nice day!')
    else:
        print('Hello! There is no logol hit in this fasta file. Have a nice day!')


if __name__=='__main__':
    main(sys.argv[1])