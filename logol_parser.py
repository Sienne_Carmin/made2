#! /usr/bin/env python3

#########################################################################
#        Script to parse a directory containing the Logol output        #
# and save matches to a fasta file as well as a csv table with figures. #
#            Usage : ./logol_parser.py /path/to/data/directory            #
#                  Written by Sarah Guinchard, may 2021                 #
#########################################################################

import xml.etree.ElementTree as et
import os
import sys
import pandas as pd
from statistics import mean

def is_match(root):
    '''Checks if the xml file contains at least one match.'''
    if root.findall('match'):
        return True
    else:
        return False
 
def get_best_match(root):
    '''Returns the sequence of the best match from the xml file.'''
    l_max = 0
    err_min = 3000
    seq = ''
    best = None
    for match in root.findall('match'):
        l = int(match.find('end').text)-int(match.find('begin').text)+1
        if int(match.find('errors').text) < err_min:
            l_max = l
            err_min = int(match.find('errors').text)
            best = match
        elif int(match.find('errors').text) == err_min & l > l_max:
            l_max = l
            err_min = int(match.find('errors').text)
            best = match
    for variable in best.findall('variable'):
        seq = seq + variable.find('content').text
    return seq, err_min, l_max

def get_fasta_header(root):
    '''Returns the fasta header of the matched sequence.'''
    return root.find('fastaHeader').text

def parse_directory(data_path):
    '''Parse all xml files in the directory containing the logol output.
       Returns a dict associating the matched sequences with the fasta headers and a DataFrame with errors and sequence length.'''
    hits = {}
    stats = {'seq':[], 'errors':[], 'size':[]}
    print('Hello!\nBeginning directory parsing...')
    for file in os.listdir(data_path):
        if file.endswith('.xml'):
            file_path = data_path+'/'+file
            root = et.parse(file_path).getroot()
            if is_match(root):
                tup = get_best_match(root)
                hits[get_fasta_header(root)] = tup[0]
                stats['errors'].append(tup[1])
                stats['size'].append(tup[2])
                stats['seq'].append(get_fasta_header(root))
    print(f'Done! From the {len(os.listdir(data_path))} xml files, {len(hits)} contained at least one match.\nMean errors: {mean(stats["errors"])}\nMin errors: {min(stats["errors"])} | Max errors: {max(stats["errors"])}\nHave a nice day!')
    return hits, pd.DataFrame(stats)

def save_to_fasta(hits, data_path):
    '''Converts the hits dict to a strings in fasta format and saves it to a file.'''
    fasta_out =''
    for header, seq in hits.items():
        fasta_out = fasta_out + '>' + header + f'_logol\n' + seq + '\n'
    with open(f'{data_path}/{os.path.basename(data_path)}_logol_results.fa', 'w') as outfile:
        outfile.write(fasta_out)

def main(data_path):
    hits, stats = parse_directory(data_path)
    save_to_fasta(hits, data_path)
    stats.to_csv(f'{data_path}/{os.path.basename(data_path)}_logol_stats.csv', index=False)

if __name__=='__main__':
    main(sys.argv[1])
