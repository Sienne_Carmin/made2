#!/bin/bash

### Source and activate your conda environment here if necessary:
#. /local/env/envconda.sh
#conda activate rm_shuffle

##### VARIABLES #####
PATHTORM=RepeatMasker
SEQUENCE=/path/to/consensus/fasta/file
GENOME=/path/to/genome/fasta/file
OUTPUTDIR=/path/to/output/directory

##### SCRIPT #####
$PATHTORM $GENOME -lib $SEQUENCE -nolow -no_is -no_id -a -dir $OUTPUTDIR

### Scipt by Sarah Guinchard (apr 2021) inspired by Lucie Baguet