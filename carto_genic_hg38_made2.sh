#!/bin/bash
##### Script written by Sarah Guinchard, june 2021
### Fill as neccessary:
ANNOTATION_INTRA=/strict/intragenic/annotation.bed
ANNOTATION_LARGE=/large/intragenic/annotation.bed
SEQUENCE_PATH=path/to/sequence/data/repo
OUTPUT_PATH=path/to/output/dir
ANNO_NAME=""
GROUP_NAME=""

# Extract annotation name for the outpout file:
ANNO_NAME=$(basename $ANNOTATION_INTRA)
ANNO_NAME=${ANNO_NAME:0:((${#ANNO_NAME}-4))}
for SEQGROUP in $SEQUENCE_PATH/hg38*.bed
do
  # Extract squences group name for the outpout file:
  GROUP_NAME=$(basename $SEQGROUP)
  GROUP_NAME=${GROUP_NAME:0:((${#GROUP_NAME}-4))}
  # Check wether the sequences matches an intergenic region:
  intersectBed -a $SEQGROUP -b $ANNOTATION_INTRA -c | awk '{if($7>0){OFS="\t"; print $4, "intra", "'$GROUP_NAME'"}}' >> $OUTPUT_PATH/$ANNO_NAME\_allclusters.csv
  intersectBed -a $SEQGROUP -b $ANNOTATION_INTRA -c | awk '{if($7==0){OFS="\t"; print $1, $2, $3, $4, $5, $6}}' > not_intra_tmp.bed
  intersectBed -a not_intra_tmp.bed -b $ANNOTATION_LARGE -c | awk '{if($7>0){OFS="\t"; print $4, "flanking", "'$GROUP_NAME'"} else{OFS="\t"; print $4, "inter", "'$GROUP_NAME'"}}'>> $OUTPUT_PATH/$ANNO_NAME\_allclusters.csv
  rm not_intra_tmp.bed
  awk '{print $4 $3}' $OUTPUT_PATH/$ANNO_NAME\_allclusters.csv | sort | uniq -c
done
