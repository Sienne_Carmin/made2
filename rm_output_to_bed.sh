#!/bin/bash

##### VARIABLES #####
### Fill in as necessary
REP=/path/to/rm/output/directory
RM_RESULTS=$REP/rm_output.out
BED_FILE=$REP/output_name.bed

##### SCRIPT #####
awk 'BEGIN { OFS="\t" } { if ($9=="+") {print $5, $6, $7, $5"forward:"$6"-"$7, $1, $9} if ($9=="C") {print $5, $6, $7, $5"reverse:"$6"-"$7, $1, "-"} }' $RM_RESULTS > $BED_FILE

### Scipt by Sarah Guinchard (apr 2021) inspired by Lucie Baguet